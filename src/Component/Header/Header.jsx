import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Market from './Market';
export default Header;

function Header(props) {
  return (
    <div className="header">
      <div className="page">
        <div className="page">Page 1</div>
      </div>
      <div className="markets">
          {props.market.map((market, i) => <Market market={market} key={i} />)}
        </div>
    </div>
  )
}

